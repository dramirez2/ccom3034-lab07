// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
// 
// Descripcion: This program calculate the net losses from Walmart's scam.
//
// ==========================================================================
#include <iostream>
#include <set>
using namespace std;

int main(){
    multiset <float> Urn; //Multiset to contains recepotn
    int n, recibos;       //No. of days (cases) and No. of receipts
    float price, total;   //Price to read 
    total = 0;            //Number of net loss of walmart
    multiset<float>::iterator it; //Iterator 

    cin >> n;	          //Reads the first case

    do {
        for (int i = 0; i < n; i++){

	    //Reads the number of receipts and does a for of that size	
	    cin >> recibos;
	    for (int j =0; j < recibos; j++){
		cin >> price;
		Urn.insert(price);
	    }
	    //Adds the total of eachs days net loss
	    total = total + (*(Urn.rbegin()) - *(Urn.begin())); 
	
	    //Erases the last and first repeceipts
	
            it = Urn.find(*(Urn.begin()));
            Urn.erase(it);
  
	    it = Urn.find(*(Urn.rbegin()));
	    Urn.erase(it);
	}
    	cout << total << endl;
	total = 0;
        cin >> n;
    } while(n != 0);
return 0;
}

